import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '../containers/Welcome.vue'
import Dashboard from '../containers/Dashboard.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/',
      name:'welcome',
      component: Welcome
    },
    {
      path:'/login',
      name:'login',
      component: Dashboard

    }
  ]
})
